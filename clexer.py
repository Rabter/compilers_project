from ply import lex
from ply.lex import TOKEN


class CLexer:
    def __init__(self):
        self.lexer = lex.lex(module=self)

    def analyze(self, data):
        res = list()
        self.lexer.input(data)
        token = self.lexer.token()
        while token:
            res.append(token)
            token = self.lexer.token()
        self.lexer.lineno = 1
        return res

    reserved = {
        'static': 'STATIC',
        'const': 'CONST',
        'volatile': 'VOLATILE',
        'restrict': 'RESTRICT',
        'extern': 'EXTERN',
        'typedef': 'TYPEDEF',
        'int': 'INT',
        'short': 'SHORT',
        'long': 'LONG',
        'float': 'FLOAT',
        'double': 'DOUBLE',
        'sizeof': 'SIZEOF',
        'void': 'VOID',
        'char': 'CHAR',
        'case': 'CASE',
        'switch': 'SWITCH',
        'break': 'BREAK',
        'return': 'RETURN',
        'continue': 'CONTINUE',
        'default': 'DEFAULT',
        'if': 'IF',
        'else': 'ELSE',
        'while': 'WHILE',
        'for': 'FOR',
        'auto': 'AUTO',
        'struct': 'STRUCT',
        'union': 'UNION',
    }

    tokens = list(reserved.values()) + \
             ['ID',
              'INT_CONST_DEC', 'INT_CONST_OCT', 'CHAR_CONST', 'FLOAT_CONST',

              'STRING_LITERAL',

              'PLUS', 'MINUS', 'ASTERISK', 'DIV', 'MOD',
              'OR', 'AND', 'NOT', 'XOR', 'LSHIFT', 'RSHIFT',
              'LOGOR', 'LOGAND', 'LOGNOT',
              'LT', 'LE', 'GT', 'GE', 'EQ', 'NE',

              'EQUALS', 'TIMESEQUAL', 'DIVEQUAL', 'MODEQUAL',
              'PLUSEQUAL', 'MINUSEQUAL',
              'LSHIFTEQUAL', 'RSHIFTEQUAL', 'ANDEQUAL', 'XOREQUAL',
              'OREQUAL',

              'INC', 'DEC',

              'ARROW',

              'TERNAR',

              'LPAREN', 'RPAREN',
              'LBRACKET', 'RBRACKET',
              'LBRACE', 'RBRACE',
              'COMMA', 'PERIOD',
              'SEMICOLON', 'COLON',
              ]

    t_PLUS = r'\+'
    t_MINUS = r'-'
    t_ASTERISK = r'\*'
    t_DIV = r'/'
    t_MOD = r'%'
    t_OR = r'\|'
    t_AND = r'&'
    t_NOT = r'~'
    t_XOR = r'\^'
    t_LSHIFT = r'<<'
    t_RSHIFT = r'>>'
    t_LOGOR = r'\|\|'
    t_LOGAND = r'&&'
    t_LOGNOT = r'!'
    t_LT = r'<'
    t_GT = r'>'
    t_LE = r'<='
    t_GE = r'>='
    t_EQ = r'=='
    t_NE = r'!='

    t_EQUALS = r'='
    t_TIMESEQUAL = r'\*='
    t_DIVEQUAL = r'/='
    t_MODEQUAL = r'%='
    t_PLUSEQUAL = r'\+='
    t_MINUSEQUAL = r'-='
    t_LSHIFTEQUAL = r'<<='
    t_RSHIFTEQUAL = r'>>='
    t_ANDEQUAL = r'&='
    t_OREQUAL = r'\|='
    t_XOREQUAL = r'\^='

    t_INC = r'\+\+'
    t_DEC = r'--'

    t_ARROW = r'->'

    t_TERNAR = r'\?'

    t_LPAREN = r'\('
    t_RPAREN = r'\)'
    t_LBRACE = r'\{'
    t_RBRACE = r'\}'
    t_LBRACKET = r'\['
    t_RBRACKET = r'\]'
    t_COMMA = r','
    t_PERIOD = r'\.'
    t_SEMICOLON = r';'
    t_COLON = r':'

    t_ignore = ' \t'

    identifier = r'[a-zA-Z_$][0-9a-zA-Z_$]*'
    integer_suffix_opt = r'(([uU]ll)|([uU]LL)|(ll[uU]?)|(LL[uU]?)|([uU][lL])|([lL][uU]?)|[uU])?'
    decimal_constant = '(0' + integer_suffix_opt + ')|([1-9][0-9]*' + integer_suffix_opt + ')'
    octal_constant = '0[0-7]*' + integer_suffix_opt
    float_constant = r"[+-]?([0-9]*[.])?[0-9]+"

    @TOKEN(identifier)
    def t_ID(self, t):
        t.type = self.reserved.get(t.value, "ID")
        return t

    @TOKEN(decimal_constant)
    def t_INT_CONST_DEC(self, t):
        return t

    @TOKEN(octal_constant)
    def t_INT_CONST_OCT(self, t):
        return t

    @TOKEN(float_constant)
    def t_FLOAT_CONST(self, t):
        return t

    def t_error(self, t):
        print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    def t_newline(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    def t_comment(self, t):
        r'/\*(.|\n)*?\*/'
        t.lexer.lineno += t.value.count('\n')

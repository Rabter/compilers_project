from llvmlite import ir
from copy import deepcopy

basic_types = {
    "int": ir.IntType(32),
    "float": ir.FloatType(),
    "double": ir.DoubleType(),
}


def get_llvm_type(type):
    if type in basic_types:
        return basic_types[type]
    # ToDo: char, string, etc
    raise ValueError("Unknown type: " + type)


def print_env(name, env):
    res = "Node: " + name + "\n"
    res += "Variables: " + str(env['vars']) + "\n"
    res += "Functions: " + str(env['funcs']) + "\n\n"
    return res


class Environment(dict):
    def __init__(self, module, builder=None):
        super().__init__()
        self.builder = builder
        self.module = module
        self.func = None
        self['vars'] = dict()
        self['funcs'] = dict()
        self['consts'] = dict()

    def clone(self):
        other = Environment(self.module, self.builder)
        other.func = self.func
        other['vars'] = deepcopy(self['vars'])
        other['funcs'] = deepcopy(self['funcs'])
        other['consts'] = deepcopy(self['consts'])
        return other


class TranslationUnit:
    def __init__(self, declarations):
        self.declarations = declarations
        self.env = None

    def build(self, module_name="C program"):
        module = ir.Module(module_name)
        self.env = Environment(module)
        for decl in self.declarations:
            decl.set_env(self.env)
            decl.build()
        return module

    def to_json(self):
        return {"Node": "Translation Unit", "Declarations": [child.to_json() for child in self.declarations]}

    def get_env(self):
        res = print_env("Translation Unit", self.env)
        for decl in self.declarations:
            res += decl.get_env()
        return res


class Declaration:
    def __init__(self, specifiers, type, dec_list):
        self.specifiers = specifiers
        self.type = type
        self.dec_list = dec_list
        self.env = None

    def build(self):
        if 'const' in self.specifiers:
            for declaration in self.dec_list:
                self.env['consts'].append(declaration)

        for declaration in self.dec_list:
            declaration.set_type(self.type)
            declaration.build()

    def set_env(self, env):
        self.env = env
        for declaration in self.dec_list:
            declaration.set_env(env)

    def to_json(self):
        obj = {
            "Node": "Declaration",
            "Specifiers": self.specifiers,
            "Type": self.type + ' (' + get_llvm_type(self.type).intrinsic_name + ')',
            "Declarators": [dec.to_json() for dec in self.dec_list]
        }
        return obj

    def get_env(self):
        res = print_env("Declaration", self.env)
        for decl in self.dec_list:
            res += decl.get_env()
        return res


class InitDeclarator:
    def __init__(self, declarator, init):
        self.declarator = declarator
        self.init = init
        self.env = None

    def to_json(self):
        obj = {
            "Node": "Initialized declarator",
            "Name": self.declarator.to_json(),
            "Value": self.init.to_json() if self.init else None
        }
        return obj

    def set_env(self, env):
        self.env = env
        self.declarator.set_env(env)
        if self.init:
            self.init.set_env(env)

    def build(self):
        name = self.declarator.name()
        if self.env.builder:
            ptr = self.env.builder.alloca(self.declarator.build(), name=name)
            if self.init:
                self.env.builder.store(self.init.build(), ptr)
        else:
            ptr = ir.GlobalVariable(self.env.module, self.declarator.build(), name=name)
            if self.init:
                ptr.initializer = self.init.build()

        self.env['vars'][name] = ptr

    def set_type(self, type):
        self.declarator.set_type(type)

    def get_env(self):
        res = print_env("Initialized declarator", self.env)
        res += self.declarator.get_env()
        if self.init:
            res += self.init.get_env()
        return res


class PointerDeclarator:
    def __init__(self, declarator, qualifiers):
        self.declarator = declarator
        self.qualifiers = qualifiers

    def to_json(self):
        return {"Node": "Pointer", "Declarator": self.declarator.to_json(), "Qualifiers": self.qualifiers}

    def set_type(self, type):
        self.declarator.set_type(type)


class Identifier:
    def __init__(self, name):
        self.name = name
        self.env = None

    def build(self):
        return self.env.builder.load(self.env['vars'][self.name])

    def ptr(self):
        return self.env['vars'][self.name]

    def to_json(self):
        return {"Node": "Identifier", "Name": self.name}

    def value(self):
        return self.env.builder.load(self.env['vars'][self.name])

    def set_env(self, env):
        self.env = env

    def get_env(self):
        return print_env("Initialized declarator", self.env)


class Constant:
    def __init__(self, type, value):
        self.type = type
        self.val = value

    def build(self):
        llvm_type = get_llvm_type(self.type)
        return ir.Constant(llvm_type, self.val)

    def to_json(self):
        obj = {
            "Node": "Constant",
            "Type": self.type + ' (' + get_llvm_type(self.type).intrinsic_name + ')',
            "Value": self.val
        }
        return obj

    def value(self):
        return self.val

    def set_env(self, _):
        pass

    def get_env(self):
        return ""


class FuncDecl:
    def __init__(self, specifiers=None, type=None, declarator=None, params=None):
        self.declarator = declarator
        self.params = list() if params is None else params
        self.specifiers = specifiers
        self.type = type

    def build(self):
        args = [parameter.build() for parameter in self.params]
        return ir.FunctionType(get_llvm_type(self.type), args)

    def to_json(self):
        obj = {
            "Node": "Function declaration",
            "Specifiers": self.specifiers,
            "Type": self.type + ' (' + get_llvm_type(self.type).intrinsic_name + ')',
            "Declarator": self.declarator.to_json(),
            "Parameters": None if self.params is None else [param.to_json() for param in self.params]
        }
        return obj


class FuncDef:
    def __init__(self, decl=None, body=None):
        self.decl = decl
        self.body = body
        self.global_env = None

    def build(self):
        name = self.decl.declarator.name()
        func = ir.Function(module=self.global_env.module, ftype=self.decl.build(), name=name)
        self.global_env['funcs'][name] = func
        if self.body:
            builder = ir.IRBuilder(func.append_basic_block())
            block_env = self.global_env.clone()
            block_env.builder = builder
            block_env.func = func
            for arg, param in zip(func.args, self.decl.params):
                arg.name = param.name()
                ptr = builder.alloca(arg.type, name=arg.name)
                block_env['vars'][arg.name] = ptr
                builder.store(arg, ptr)
            self.body.set_env(block_env)
            self.body.build()

    def to_json(self):
        return {"Node": "Function Definition", "Declaration": self.decl.to_json(), "Body": self.body.to_json()}

    def set_env(self, env):
        self.global_env = env

    def get_env(self):
        res = print_env("Function Definition", self.global_env)
        res += self.body.get_env()
        return res


class Parameter:
    def __init__(self, type, declarator):
        self.type = type
        self.declarator = declarator

    def build(self):
        return get_llvm_type(self.type)

    def to_json(self):
        obj = {
            "Node": "Parameter",
            "Type": self.type + ' (' + get_llvm_type(self.type).intrinsic_name + ')',
            "Declarator": self.declarator.to_json()
        }
        return obj

    def name(self):
        return self.declarator.name()


class CompoundStatement:
    def __init__(self, body):
        self.body = body

    def build(self):
        for entry in self.body:
            entry.build()

    def set_env(self, env):
        block_env = env.clone()
        for entry in self.body:
            entry.set_env(block_env)

    def to_json(self):
        return {"Node": "Compound Statement", "Children": [child.to_json() for child in self.body]}

    def get_env(self):
        res = ""
        for entry in self.body:
            res += entry.get_env()
        return res


class Return:
    def __init__(self, expression=None):
        self.expression = expression
        self.env = None

    def set_env(self, env):
        self.env = env
        self.expression.set_env(env)

    def to_json(self):
        return {"Node": "Return statement", "Returning value": self.expression.to_json() if self.expression else None}

    def build(self):
        if self.expression:
            self.env.builder.ret(self.expression.build())
        else:
            self.env.builder.ret_void()

    def get_env(self):
        res = print_env("Return Statement", self.env)
        res += self.expression.get_env()
        return res


class FuncCall:
    def __init__(self, identifier, args=None):
        self.identifier = identifier
        self.args = args
        self.env = None

    def set_env(self, env):
        self.env = env
        self.identifier.set_env(env)
        if self.args:
            for arg in self.args:
                arg.set_env(env)

    def to_json(self):
        obj = {
            "Node": "Function call",
            "Identifier": self.identifier.to_json(),
            "Argument list": [arg.to_json() for arg in self.args] if self.args else None
        }
        return obj

    def build(self):
        fn = self.env['funcs'][self.identifier.name]
        if self.args is None:
            args = list()
        else:
            args = [arg.build() for arg in self.args]
        return self.env.builder.call(fn, args)

    def get_env(self):
        res = print_env("Function call", self.env)
        res += self.identifier.get_env()
        if self.args:
            for arg in self.args:
                res += arg.get_env()
        return res


class IdentifierDeclarator:
    def __init__(self, identifier, type=None):
        self.identifier = identifier
        self.type = type
        self.env = None

    def set_env(self, env):
        self.env = env
        self.identifier.env = env

    def to_json(self):
        return {"Node": "Declarator", "Identifier": self.identifier.to_json()}

    def build(self):
        return get_llvm_type(self.type)

    def name(self):
        return self.identifier.name

    def set_type(self, type):
        self.type = type

    def get_env(self):
        res = print_env("Declarator", self.env)
        res += self.identifier.get_env()
        return res


class ArrayDecl:
    def __init__(self, declarator, dimension=None):
        self.declarator = declarator
        self.dimension = dimension
        self.env = None

    def to_json(self):
        obj = {
            "Node": "Array Declaration",
            "Declarator": self.declarator.to_json(),
            "Dimension": self.dimension.to_json()
        }
        return obj

    def build(self):
        t = ir.ArrayType(self.declarator.build(), int(self.dimension.value()))
        return t

    def name(self):
        return self.declarator.name()

    def set_env(self, env):
        self.env = env

    def set_type(self, type):
        self.declarator.set_type(type)

    def get_env(self):
        return print_env("Array Declaration", self.env)


class ArrayAccessing:
    def __init__(self, identifier, index):
        self.identifier = identifier
        self.index = index
        self.env = None

    def build(self):
        return self.env.builder.load(
            self.env.builder.gep(self.identifier.ptr(), [ir.Constant(ir.IntType(32), 0), self.index.build()]))

    def ptr(self):
        return self.env.builder.gep(self.identifier.ptr(), [ir.Constant(ir.IntType(32), 0), self.index.build()])

    def set_env(self, env):
        self.env = env
        self.identifier.set_env(env)
        self.index.set_env(env)

    def to_json(self):
        return {"Node": "Array accessing", "Identifier": self.identifier.to_json(), "Index": self.index.to_json()}

    def get_env(self):
        res = print_env("Array accessing", self.env)
        res += self.identifier.get_env()
        res += self.index.get_env()
        return res


class BinaryExpr:
    def __init__(self, operator, left, right):
        self.op = operator
        self.left = left
        self.right = right
        self.env = None

    def build(self):
        left = self.left.build()
        right = self.right.build()
        if self.op == '+':
            return self.env.builder.add(left, right)
        elif self.op == '-':
            return self.env.builder.sub(left, right)
        elif self.op == '*':
            return self.env.builder.mul(left, right)
        elif self.op == '/':
            return self.env.builder.sdiv(left, right)  # ToDo: float division
        elif self.op == '%':
            return self.env.builder.srem(left, right)
        elif self.op in {'<', '<=', '==', '!=', '>=', '>'}:
            return self.env.builder.icmp_signed(self.op, left, right)
        elif self.op == '&&':
            left = self.env.builder.icmp_signed('!=', left, ir.Constant(ir.IntType(1), 0))
            right = self.env.builder.icmp_signed('!=', right, ir.Constant(ir.IntType(1), 0))
            return self.env.builder.and_(left, right)
        elif self.op == '||':
            left = self.env.builder.icmp_signed('!=', left, ir.Constant(ir.IntType(1), 0))
            right = self.env.builder.icmp_signed('!=', right, ir.Constant(ir.IntType(1), 0))
            return self.env.builder.or_(left, right)

    def set_env(self, env):
        self.env = env
        self.left.set_env(env)
        self.right.set_env(env)

    def to_json(self):
        obj = {
            "Node": "Binary operator",
            "Operator": self.op,
            "Left": self.left.to_json(),
            "Right": self.right.to_json()
        }
        return obj

    def get_env(self):
        res = print_env("Binary operator", self.env)
        res += self.left.get_env()
        res += self.right.get_env()
        return res


class UnaryOperator:
    def __init__(self, operator, expression, left=True):
        self.op = operator
        self.expr = expression
        self.left = left
        self.env = None

    def set_env(self, env):
        self.env = env
        self.expr.set_env(env)

    def build(self):
        pass  # ToDo

    def to_json(self):
        obj = {
            "Node": "Unary Operator",
            "Operator": self.op,
            "Expression": self.expr.to_json(),
            "Position": "Left" if self.left else "Right"
        }
        return obj

    def get_env(self):
        res = print_env("Unary Operator", self.env)
        res += self.expr.get_env()
        return res


class Assignment:
    def __init__(self, operator, left, right):
        self.op = operator
        self.left = left
        self.right = right
        self.env = None

    def build(self):
        if self.op == '=':
            ptr = self.left.ptr()
            val = self.right.build()
        else:
            raise ValueError("Unimplemented assignment operator")
        self.env.builder.store(val, ptr)

    def set_env(self, env):
        self.env = env
        self.left.set_env(env)
        self.right.set_env(env)

    def to_json(self):
        obj = {
            "Node": "Assignment operator",
            "Operator": self.op,
            "Left": self.left.to_json(),
            "Right": self.right.to_json()
        }
        return obj

    def get_env(self):
        res = print_env("Assignment operator", self.env)
        res += self.left.get_env()
        res += self.right.get_env()
        return res


class ExpressionStatement:
    def __init__(self, expression_list):
        self.expressions = expression_list
        self.env = None

    def set_env(self, env):
        self.env = env
        for expr in self.expressions:
            expr.set_env(env)

    def build(self):
        for expr in self.expressions:
            expr.build()

    def to_json(self):
        obj = {
            "Node": "Expression Statement",
            "Statements": [expr.to_json() for expr in self.expressions]
        }
        return obj

    def get_env(self):
        res = print_env("Expression Statement", self.env)
        for expr in self.expressions:
            res += expr.get_env()
        return res


class CondOperator:
    def __init__(self, condition, true_case, false_case=None):
        self.cond = condition
        self.tcase = true_case
        self.fcase = false_case
        self.env = None

    def to_json(self):
        obj = {
            "Node": "Condition Operator",
            "Condition": self.cond.to_json(),
            "True case": self.tcase.to_json(),
            "False case": self.fcase.to_json() if self.fcase else None
        }
        return obj

    def set_env(self, env):
        self.env = env
        self.cond.set_env(env)

    def build(self):
        self.tcase.set_env(self.env)
        if self.fcase:
            self.fcase.set_env(self.env)
        cond = self.env.builder.icmp_signed('!=', self.cond.build(), ir.Constant(ir.IntType(1), 0))
        if self.fcase:
            with self.env.builder.if_else(cond) as (then, otherwise):
                with then:
                    self.tcase.build()
                with otherwise:
                    self.fcase.build()
        else:
            with self.env.builder.if_then(cond):
                self.tcase.build()

    def get_env(self):
        res = print_env("Condition Operator", self.env)
        res += self.cond.get_env()
        res += self.tcase.get_env()
        if self.fcase:
            res += self.fcase.get_env()
        return res


class While:
    def __init__(self, condition, statement):
        self.cond = condition
        self.statement = statement
        self.env = None

    def to_json(self):
        obj = {
            "Node": "While loop",
            "Condition": self.cond.to_json(),
            "Statement": self.statement.to_json(),
        }
        return obj

    def set_env(self, env):
        self.env = env
        self.cond.set_env(env)

    def build(self):
        self.statement.set_env(self.env)

        label_cond = self.env.func.append_basic_block()
        label_statement = self.env.func.append_basic_block()
        label_end = self.env.func.append_basic_block()

        self.env.builder.branch(label_cond)
        self.env.builder.position_at_start(label_cond)
        cond = self.env.builder.icmp_signed('!=', self.cond.build(), ir.Constant(ir.IntType(1), 0))
        self.env.builder.cbranch(cond, label_statement, label_end)

        self.env.builder.position_at_start(label_statement)
        self.statement.build()
        self.env.builder.branch(label_cond)

        self.env.builder.position_at_start(label_end)

    def get_env(self):
        res = print_env("While loop", self.env)
        res += self.cond.get_env()
        res += self.statement.get_env()
        return res


class For:
    def __init__(self, decl_expr=None, condition=None, it_expr=None, statement=None):
        self.decl_expr = decl_expr
        self.cond = condition
        self.it_expr = it_expr
        self.statement = statement
        self.env = None

    def to_json(self):
        obj = {
            "Node": "For loop",
            "Declaration expression": self.decl_expr.to_json(),
            "Condition": self.cond.to_json(),
            "Iteration expression": self.it_expr.to_json(),
            "Statement": self.statement.to_json(),
        }
        return obj

    def set_env(self, env):
        self.env = env
        if self.cond:
            self.cond.set_env(env)

    def build(self):
        if self.decl_expr:
            self.decl_expr.set_env(self.env)
        if self.it_expr:
            self.it_expr.set_env(self.env)

        label_cond = self.env.func.append_basic_block()
        label_it = self.env.func.append_basic_block()
        label_statement = self.env.func.append_basic_block()
        label_end = self.env.func.append_basic_block()

        if self.decl_expr:
            self.decl_expr.build()

        self.env.builder.branch(label_cond)
        self.env.builder.position_at_start(label_cond)
        if self.cond:
            cond = self.env.builder.icmp_signed('!=', self.cond.build(), ir.Constant(ir.IntType(1), 0))
            self.env.builder.cbranch(cond, label_statement, label_end)
        else:
            self.env.builder.branch(label_statement)

        self.statement.set_env(self.env)
        self.env.builder.position_at_start(label_statement)
        self.statement.build()
        self.env.builder.branch(label_it)

        self.env.builder.position_at_start(label_it)
        if self.it_expr:
            self.it_expr.build()
        self.env.builder.branch(label_cond)

        self.env.builder.position_at_start(label_end)

    def get_env(self):
        res = print_env("For Loop", self.env)
        if self.cond:
            res += self.cond.get_env()
        if self.decl_expr:
            res += self.decl_expr.get_env()
        if self.it_expr:
            res += self.it_expr.get_env()
        res += self.statement.get_env()
        return res

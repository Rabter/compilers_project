int main()
{
    int arr[5];
    arr[0] = 5;
    arr[1] = 2;
    arr[2] = 3;
    arr[3] = 4;
    arr[4] = 1;

    int res = 0;

    for (int i = 0; i < 5; i = i + 1)
    {
        int min = arr[i], imin = i;
        for (int j = i + 1; j < 5; j = j + 1)
        {
            if (arr[j] < min)
            {
                min = arr[j];
                imin = j;
            }
        }
        int tmp = arr[i];
        arr[i] = arr[imin];
        arr[imin] = tmp;
        res = res * 10;
        res = res + arr[i];
    }
    return res;
}
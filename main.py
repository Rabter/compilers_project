import sys

from clexer import CLexer
from cparser import CParser
from json import dumps

if __name__ == "__main__":
    filename = "test.c"
    path = sys.argv[1] if len(sys.argv) > 1 else "cfiles/" + filename
    with open(path) as fin:
        data = fin.read()

    lexer = CLexer()
    parser = CParser()
    tokens = lexer.analyze(data)
    print(tokens)

    AST = parser.parse(data, lexer.lexer)
    json_AST = AST.to_json()
    json_AST = dumps(json_AST, indent=4)  # Only adding indentation
    print(json_AST)

    left = path.rfind('\\') + 1
    if left == 0:
        left = path.rfind('/') + 1
    path = "builds/" + path[left:path.rfind('.')] + '.ll'
    with open(path, "w") as fout:
        fout.write(json_AST)

    ir_module = str(AST.build(filename))
    print(ir_module)

    print(AST.get_env())

    path = sys.argv[2] if len(sys.argv) > 2 else "builds/" + path[left:path.rfind('.')] + '.ll'
    with open(path, "w") as fout:
        fout.write(ir_module)

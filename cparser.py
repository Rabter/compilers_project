import sys

from ply import yacc
from ply.yacc import PlyLogger

from clexer import CLexer
import AST


class CParser:
    def __init__(self):
        self.tokens = CLexer.tokens
        self.cparser = yacc.yacc(module=self,
                                 start='translation_unit',
                                 debug=False, errorlog=PlyLogger(sys.stdout))

    def parse(self, data, lexer):
        return self.cparser.parse(input=data, lexer=lexer)

    precedence = (
        ('left', 'LOGOR'),
        ('left', 'LOGAND'),
        ('left', 'OR'),
        ('left', 'XOR'),
        ('left', 'AND'),
        ('left', 'EQ', 'NE'),
        ('left', 'LT', 'LE', 'GE', 'GT'),
        ('left', 'LSHIFT', 'RSHIFT'),
        ('left', 'PLUS', 'MINUS'),
        ('left', 'ASTERISK', 'DIV', 'MOD'),
        ('left', 'INC', 'DEC', 'PERIOD', 'ARROW'),
    )

    def p_type_qualifier(self, p):
        """type_qualifier : CONST
                          | VOLATILE
                          | RESTRICT"""

        p[0] = p[1]

    def p_storage_class_specifier(self, p):
        """storage_class_specifier : AUTO
                                   | STATIC
                                   | EXTERN
                                   | TYPEDEF"""
        p[0] = p[1]

    def p_assignment_operator(self, p):
        """assignment_operator : EQUALS
                               | TIMESEQUAL
                               | DIVEQUAL
                               | MODEQUAL
                               | PLUSEQUAL
                               | MINUSEQUAL
                               | LSHIFTEQUAL
                               | RSHIFTEQUAL
                               | ANDEQUAL
                               | XOREQUAL
                               | OREQUAL"""
        p[0] = p[1]

    def p_unary_operator(self, p):
        """unary_operator : AND
                          | ASTERISK
                          | PLUS
                          | MINUS
                          | NOT
                          | LOGNOT"""
        p[0] = p[1]

    def p_struct_or_union(self, p):
        """struct_or_union : STRUCT
                           | UNION"""
        p[0] = p[1]

    def p_translation_unit(self, p):
        """translation_unit : external_declaration_list"""
        p[0] = AST.TranslationUnit(p[1])

    def p_external_declaration_list(self, p):
        """external_declaration_list : external_declaration
                                     | external_declaration external_declaration_list"""
        p[0] = [p[1]]
        if len(p) == 3 and p[2] is not None:
            p[0].extend(p[2])

    def p_external_declaration(self, p):
        """external_declaration : function_definition
                                | declaration"""
        p[0] = p[1]

    def p_function_definition_1(self, p):
        """function_definition : type_specifiers declarator compound_statement"""
        p[2].spec = list()
        p[2].type = ' '.join(p[1])
        p[0] = AST.FuncDef(p[2], p[3])

    def p_function_definition_2(self, p):
        """function_definition : declaration_specifiers type_specifiers declarator compound_statement"""
        p[3].spec = p[1]
        p[3].type = ' '.join(p[2])
        p[0] = AST.FuncDef(p[3], p[4])

    def p_block_item(self, p):
        """block_item : declaration
                      | statement"""
        p[0] = p[1]

    def p_block_item_list(self, p):
        """block_item_list : block_item
                           | block_item block_item_list"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[2]
            p[0].insert(0, p[1])

    def p_compound_statement(self, p):
        """compound_statement : LBRACE block_item_list RBRACE
                              | LBRACE RBRACE"""
        if len(p) == 4:
            p[0] = AST.CompoundStatement(p[2])
        else:
            p[0] = AST.CompoundStatement(list())

    def p_statement(self, p):
        """statement   : compound_statement
                       | expression_statement
                       | selection_statement
                       | jump_statement
                       | iteration_statement"""
        # | labeled_statement
        p[0] = p[1]

    def p_expression_statement(self, p):
        """expression_statement : expression_list SEMICOLON"""
        p[0] = AST.ExpressionStatement(p[1])


    def p_declaration(self, p):
        """declaration : declaration_specifiers type_specifiers init_declarator_list SEMICOLON
                       | type_specifiers init_declarator_list SEMICOLON"""
        if len == 5:
            p[0] = AST.Declaration(specifiers=p[1], type=' '.join(p[2]), dec_list=p[3])
        else:
            p[0] = AST.Declaration(specifiers=list(), type=' '.join(p[1]), dec_list=p[2])

    def p_declaration_specifiers(self, p):
        """declaration_specifiers : declaration_specifier
                                  | declaration_specifiers declaration_specifier"""
        p[0] = p[1] + [p[2]] if len(p) == 3 else [p[1]]

    def p_type_specifier(self, p):
        """type_specifier : VOID
                         | CHAR
                         | SHORT
                         | INT
                         | LONG
                         | FLOAT
                         | DOUBLE"""
        # ToDo enum and struct+union
        p[0] = p[1]

    def p_type_specifiers(self, p):
        """type_specifiers : type_specifier
                           | type_specifiers type_specifier"""
        p[0] = p[1] + [p[2]] if len(p) == 3 else [p[1]]

    def p_type_qualifiers(self, p):
        """type_qualifiers : type_qualifier
                           | type_qualifiers type_qualifier"""
        p[0] = p[1] + [p[2]] if len(p) == 3 else [p[1]]

    def p_init_declarator(self, p):
        """init_declarator : declarator
                           | declarator EQUALS initializer"""
        init = p[3] if len(p) == 4 else None
        p[0] = AST.InitDeclarator(p[1], init)

    def p_declarator(self, p):
        """declarator : direct_declarator"""
        p[0] = p[1]

    def p_declarator_pointer(self, p):
        """declarator : pointer direct_declarator"""
        p[1].declarator = p[2]
        p[0] = p[1]

    def p_pointer(self, p):
        """pointer : ASTERISK
                   | ASTERISK pointer"""
        p[0] = AST.PointerDeclarator(declarator=None, qualifiers=list())
        if len(p) == 3:
            p[2].declarator = p[0]

    def p_pointer_qual(self, p):
        """pointer : ASTERISK type_qualifiers
                   | ASTERISK type_qualifiers pointer"""
        p[0] = AST.PointerDeclarator(declarator=None, qualifiers=p[2])
        if len(p) == 4:
            p[3].declarator = p[0]

    def p_identifier(self, p):
        """identifier : ID"""
        p[0] = AST.Identifier(p[1])

    def p_identifier_list(self, p):
        """identifier_list : identifier
                           | identifier COMMA identifier_list"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[3]
            p[0].insert(0, p[1])

    def p_direct_declarator(self, p):
        """direct_declarator : identifier"""
        p[0] = AST.IdentifierDeclarator(p[1])

    def p_direct_declarator_func_1(self, p):
        """direct_declarator : direct_declarator LPAREN RPAREN"""
        p[0] = AST.FuncDecl(declarator=p[1])

    def p_parameter(self, p):
        """parameter : type_specifiers declarator"""
        p[0] = AST.Parameter(' '.join(p[1]), p[2])

    def p_parameter_type_list(self, p):
        """parameter_type_list : parameter
                               | parameter COMMA parameter_type_list"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[3]
            p[0].insert(0, p[1])

    def p_direct_declarator_func_2(self, p):
        """direct_declarator : direct_declarator LPAREN parameter_type_list RPAREN"""
        p[0] = AST.FuncDecl(declarator=p[1], params=p[3])

    def p_direct_declarator_arr_1(self, p):
        """direct_declarator : direct_declarator LBRACKET RBRACKET"""
        p[0] = AST.ArrayDecl(declarator=p[1])

    def p_direct_declarator_arr_2(self, p):
        """direct_declarator : direct_declarator LBRACKET assignment_expression RBRACKET"""
        p[0] = AST.ArrayDecl(declarator=p[1], dimension=p[3])

    def p_init_declarator_list(self, p):
        """init_declarator_list : init_declarator
                                | init_declarator_list COMMA init_declarator"""
        p[0] = p[1] + [p[3]] if len(p) == 4 else [p[1]]

    def p_initializer(self, p):
        """initializer : assignment_expression"""
        p[0] = p[1]

    def p_assignment_expression(self, p):
        """assignment_expression : binary_expression
                                 | unary_expression assignment_operator assignment_expression"""
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = AST.Assignment(p[2], p[1], p[3])

    def p_constant_int(self, p):
        """constant : INT_CONST_DEC
                    | INT_CONST_OCT"""
        p[0] = AST.Constant('int', p[1])

    def p_constant_char(self, p):
        """constant : CHAR_CONST"""
        p[0] = AST.Constant('char', p[1])

    def p_constant_float(self, p):
        """constant : FLOAT_CONST"""
        p[0] = AST.Constant('float', p[1])

    def p_primary_expression(self, p):
        """primary_expression : identifier
                              | constant"""
        # | string_literal
        p[0] = p[1]

    # def p_primary_expression_1(self, p):
    #     """primary_expression : LPAREN expression RPAREN"""
    #     p[0] = p[2]

    # def p_generic_assoc_list(self, p):
    #     """generic_assoc_list : generic_association
    #                           | generic_association COMMA generic_association_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])

    # def p_generic_association(self, p):
    #     """generic_association : type_name COLON
    #                            | assignment_expression"""
    #     p[0] = p[1]

    def p_argument_expression_list(self, p):
        """argument_expression_list : assignment_expression
                                    | assignment_expression COMMA argument_expression_list"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[3]
            p[0].insert(p[1], 0)

    # def p_and_expression(self, p):
    #     """and_expression : equality_expression
    #                       | equality_expression AND equality_expression_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])
    #
    # def p_exclusive_or_expression(self, p):
    #     """exclusive_or_expression : and_expression
    #                                | and_expression XOR and_expression_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])
    #
    # def p_inclusive_or_expression(self, p):
    #     """inclusive_or_expression : exclusive_or_expression
    #                                | exclusive_or_expression OR exclusive_or_expression_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])
    #
    # def p_logical_and_expression(self, p):
    #     """logical_and_expression : inclusive_or_expression
    #                               | inclusive_or_expression LOGAND inclusive_or_expression_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])
    #
    # def p_logical_or_expression(self, p):
    #     """logical_or_expression : logical_and_expression
    #                              | logical_and_expression LOGOR logical_and_expression_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])

    def p_binary_expression(self, p):
        """binary_expression : unary_expression
                             | binary_expression ASTERISK binary_expression
                             | binary_expression DIV binary_expression
                             | binary_expression MOD binary_expression
                             | binary_expression PLUS binary_expression
                             | binary_expression MINUS binary_expression
                             | binary_expression RSHIFT binary_expression
                             | binary_expression LSHIFT binary_expression
                             | binary_expression LT binary_expression
                             | binary_expression LE binary_expression
                             | binary_expression GE binary_expression
                             | binary_expression GT binary_expression
                             | binary_expression EQ binary_expression
                             | binary_expression NE binary_expression
                             | binary_expression AND binary_expression
                             | binary_expression OR binary_expression
                             | binary_expression XOR binary_expression
                             | binary_expression LOGAND binary_expression
                             | binary_expression LOGOR binary_expression
        """
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = AST.BinaryExpr(p[2], p[1], p[3])

    def p_empty(self, p):
        """empty : """
        p[0] = None

    def p_expression(self, p):
        """expression : assignment_expression"""
        p[0] = p[1]

    def p_expression_opt(self, p):
        """expression_opt : expression
                          | empty"""
        p[0] = p[1]

    def p_expression_list(self, p):
        """expression_list : expression
                           | expression COMMA expression"""
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[3]
            p[0].insert(p[1])

    def p_declaration_specifier(self, p):
        """declaration_specifier : storage_class_specifier
                                 | type_qualifier"""
        p[0] = p[1]

    # def p_struct_declarator_list(self, p):
    #     """struct_declarator_list : struct_declarator
    #                               | struct_declarator COMMA struct_declarator_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])

    # def p_enumerator_list(self, p):
    #     """enumerator_list : enumerator
    #                        | enumerator COMMA enumerator_list"""
    #     if len(p) == 2:
    #         p[0] = [p[1]]
    #     else:
    #         p[0] = p[3]
    #         p[0].insert(p[1])

    def p_unary_expression_1(self, p):
        """unary_expression : postfix_expression"""
        p[0] = p[1]

    def p_unary_expression_2(self, p):
        """unary_expression : INC unary_expression
                            | DEC unary_expression
                            | SIZEOF unary_expression"""
        # | unary_operator cast_expression"""
        p[0] = AST.UnaryOperator(p[1], p[2], left=True)

    def p_postfix_expression(self, p):
        """postfix_expression : primary_expression"""
        p[0] = p[1]

    def p_postfix_expression_arr(self, p):
        """postfix_expression : identifier LBRACKET expression RBRACKET"""
        p[0] = AST.ArrayAccessing(p[1], p[3])

    def p_postfix_expression_func(self, p):
        """postfix_expression  : identifier LPAREN argument_expression_list RPAREN
                                | identifier LPAREN RPAREN"""
        p[0] = AST.FuncCall(p[1], p[3] if len(p) == 5 else None)

    def p_postfix_expression_struct(self, p):
        """postfix_expression : postfix_expression PERIOD identifier
                              | postfix_expression ARROW identifier"""
        pass  # ToDo

    def p_postfix_expression_incdec(self, p):
        """postfix_expression : postfix_expression INC
                              | postfix_expression DEC"""
        p[0] = AST.UnaryOperator(p[2], p[1], left=False)

    def p_selection_statement_1(self, p):
        """selection_statement : IF LPAREN expression RPAREN statement"""
        p[0] = AST.CondOperator(p[3], p[5], None)

    def p_selection_statement_2(self, p):
        """selection_statement : IF LPAREN expression RPAREN statement ELSE statement"""
        p[0] = AST.CondOperator(p[3], p[5], p[7])

    def p_jump_statement_ret(self, p):
        """jump_statement : RETURN expression SEMICOLON
                          | RETURN SEMICOLON"""
        p[0] = AST.Return(p[2] if len(p) == 4 else None)

    def p_iteration_statement_while(self, p):
        """iteration_statement : WHILE LPAREN expression RPAREN statement"""
        p[0] = AST.While(p[3], p[5])

    def p_iteration_statement_for(self, p):
        """iteration_statement : FOR LPAREN expression_opt SEMICOLON expression_opt SEMICOLON expression_opt RPAREN statement
                               | FOR LPAREN for_declaration SEMICOLON expression_opt SEMICOLON expression_opt RPAREN statement"""
        p[0] = AST.For(p[3], p[5], p[7], p[9])

    def p_for_declaration(self, p):
        """for_declaration : type_specifiers init_declarator_list"""
        p[0] = AST.Declaration(specifiers=list(), type=' '.join(p[1]), dec_list=p[2])
